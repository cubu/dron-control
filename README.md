# Dron Control

Dron control to define routes and movements for drones inside a defined rectangle.

## Getting Started

To run the project enter the directory and type:

```
npm install

npm start
```

After being prompted with "Input:" the first line will define the rectangle where the drones will be able to fly. It expects a line with this format: "X Y" where {X, Y} are numbers.

From now one each pair of lines will define the drone position in the rectangle and the drone route.

The drone position will expect this format: "X Y Z" where {X, Y} are numbers and {Z} is a cardinal position (N, E, S, O).

The drone route will expect this format: "XXXXXXX..." where {X} represents:
    - "L" -> Rotate left
    - "R" -> Rotate right
    - "M" -> Move forward towards the current dron direction

You can input any drones as you like, and to finish you have to enter and empty line, after that you will be prompted with the Output.

## Examples

```
npm start

Input:
5 5
3 3 E
L

Output:
3 3 N
```

```
npm start

Input:
5 5
3 3 E
L
3 3 E
MMRMMRMRRM
1 2 N
LMLMLMLMMLMLMLMLMM

Output:
3 3 N
5 1 E
1 4 N
```

## Running the tests

To run the tests type:

```
npm test
```

## Built With

* [Node](https://nodejs.org/en/) - JavaScript runtime environment
* [npm](https://www.npmjs.com/) - Node Package Manager
* [Jest](https://jestjs.io/) - Testing platform for JavaScript
