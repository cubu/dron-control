var line = 0;
var rect = [];
var currentDron = {};
var drones = [];
var cardinals = ["N", "E", "S", "O"];

var processLine = (input) => {
    var res = false;
    if(input === '')
    {
        res = processInput(drones);
    }else
    {
        exportFunctions.readLineInput(input);
    }
    return res;
};

var readLineInput = (input) => {
    line++;
    if(line==1)
    {
      rect = input.split(" ");
      if(rect.length !== 2){
          line--;
          console.log('Please read the readme file, and input the first line properly:')
      }
    }
    else if(line%2 === 0)
    {
      var inpArray = input.split(" ");
      currentDron.position = inpArray.splice(0, inpArray.length - 1);
      currentDron.direction = inpArray.shift();
      if(currentDron.position.length !== 2 
        || !cardinals.includes(currentDron.direction.toUpperCase())
        || currentDron.position[0] >= rect[0]
        || currentDron.position[1] >= rect[1]){
        line--;
        console.log('Please read the readme file, and input the drone position properly:')
      }
    }
    else
    {
      currentDron.route = input.split("");
      drones.push(currentDron);
      currentDron = {};
    }
};

var processInput = (drones) => {
    console.log("Output:");
    drones.forEach((dron) => {
        dron.route.forEach((mov)=>{
            switch(mov.toLowerCase())
            {
                case "m":
                    move(dron);
                break;
                default:
                    rotate(dron, mov.toLowerCase());
                break;
            }
        });
    });
    listDrones(drones);
    return true;
};

var move = (currentDron) => {
    switch(currentDron.direction.toLowerCase())
    {
        case "n":
            if(currentDron.position[1] < (rect[1] - 1)){
                currentDron.position[1]++;
            }
        break;
        case "e":
            if(currentDron.position[0] < (rect[0] - 1)){
                currentDron.position[0]++;
            }
        break;
        case "s":
            if(currentDron.position[1] > 0){
                currentDron.position[1]--;
            }
        break;
        case "o":
            if(currentDron.position[0] > 0){
                currentDron.position[0]--;
            }
        break;
    }
};

var rotate = (currentDron, direction) => {
    switch(direction.toLowerCase())
    {
        case "l":
            var c = cardinals.indexOf(currentDron.direction.toUpperCase());
            var newDirectionArray = arrayRotate(cardinals, c);
            newDirectionArray = arrayRotate(newDirectionArray, -1);
            currentDron.direction = newDirectionArray[0];
        break;
        case "r":
            var c = cardinals.indexOf(currentDron.direction.toUpperCase());
            var newDirectionArray = arrayRotate(cardinals, c);
            newDirectionArray = arrayRotate(newDirectionArray, 1);
            currentDron.direction = newDirectionArray[0];
        break;
    }
};

var arrayRotate = ((arr, count)=>{
    count -= arr.length * Math.floor(count / arr.length)
    arr.push.apply(arr, arr.splice(0, count))
    return arr
});

var listDrones = (list) =>{
    list.forEach((dron) => {
      console.log(`${dron.position} ${dron.direction}`);
    });
  };

var getDrones = () => {
    return drones;
};

var getRectangle = () => {
    return rect;
};

//In order to mock function in test file
const exportFunctions = {
    readLineInput
};

module.exports = {
    processInput,
    move,
    rotate,
    exportFunctions,
    processLine,
    getDrones,
    getRectangle
  };