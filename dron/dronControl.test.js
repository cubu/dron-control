var dronControl = require('./dronControl.js');

test('should move a position', () => {
    var input = "5 5";

    dronControl.processLine(input);

    var currentDron = {};
    
    currentDron.position = [0, 0];
    currentDron.direction = "N";

    dronControl.move(currentDron);

    expect(currentDron.position).toEqual([0, 1]);
});

test('should rotate', () => {
    var currentDron = {};
    currentDron.position = [0, 0];
    currentDron.direction = "N";

    dronControl.rotate(currentDron, "L");

    expect(currentDron.direction).toBe("O");
});

test('should read a drone and a rectangle', () => {
    var input = "5 5";

    dronControl.exportFunctions.readLineInput(input);

    input = "3 3 E";

    dronControl.exportFunctions.readLineInput(input);

    input = "L";

    dronControl.exportFunctions.readLineInput(input);

    var dron = dronControl.getDrones()[0];

    expect(dron).toEqual(expect.objectContaining({
        position: [expect.any(String), expect.any(String)],
        direction: expect.any(String),
        route:[expect.any(String)]
    }));

    var rect = dronControl.getRectangle();

    expect(rect).toEqual(expect.arrayContaining(["5", "5"]));

});

test('should call readLineInput 3 times', () => {
    dronControl.exportFunctions.readLineInput = jest.fn();
    var input = "5 5";

    dronControl.processLine(input);

    input = "3 3 E";

    dronControl.processLine(input);

    input = "L";

    dronControl.processLine(input);

    expect(dronControl.exportFunctions.readLineInput)
    .toHaveBeenCalledTimes(3);

    expect(dronControl.exportFunctions.readLineInput)
    .toHaveBeenCalledWith("5 5");

    expect(dronControl.exportFunctions.readLineInput)
    .toHaveBeenCalledWith("3 3 E");

    expect(dronControl.exportFunctions.readLineInput)
    .toHaveBeenCalledWith("L");
});
