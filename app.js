
const readline = require('readline');
const dronControl = require('./dron/dronControl.js');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

console.log("Input:");

rl.on('line', (input) => {
  var res = dronControl.processLine(input);

  if(res){
    rl.close();
  }
});